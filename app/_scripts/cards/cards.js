
const cardGenerator = (function makeCard() {
    return {
        createCard: function createCard(card) {
            return`<article class="card">
                <header class="card-header noselect"><input type="checkbox" id="box${card.id}">
                    <label for="box${card.id}">
                        <span>${card.name}</span>
                    </label>
                    <p>${card.number}</p>
                </header>
                <figure class="card-picture noselect">
                    <img src="public/images/cards/dna-image.png"><a href="#">${card.moleculeName}</a>
                </figure>
                <div class="card-tool-bar noselect">
                    <ul>
                        <li><a href="#"><img src="public/images/cards/pen.png"></a></li>
                        <li><a href="#"><img src="public/images/cards/l-plus-sign.png"></a></li>
                        <li><a href="#"><img src="public/images/cards/arrow-left.png"></a></li>
                        <li><a href="#"><img src="public/images/cards/arrow-right.png"></a></li>
                    </ul>
                </div>
                <ul class="card-info">
                    <li><a href="#">${card.properties[0].propName}</a><span>${card.properties[0].propNumber}</span></li>
                    <li><a href="#">${card.properties[1].propName}</a><span>${card.properties[1].propNumber}</span></li>
                    <li><a href="#">${card.properties[2].propName}</a><span>${card.properties[2].propNumber}</span></li>
                    <li><a href="#">${card.properties[3].propName}</a><span>${card.properties[3].propNumber}</span></li>
                </ul>
            </article>`
        }
    }
}());