document.addEventListener("DOMContentLoaded", function () {
    loadCards();
});

function loadCards() {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', '/data/cards.json', true);
    xhr.send();
    xhr.onreadystatechange = function () {
        if (xhr.readyState != 4) {
            return;
        }
        let cards;
        if (xhr.status != 200) {
            console.log(xhr.status + ': ' + xhr.statusText);
        } else {
            try {
                cards = JSON.parse(xhr.responseText);
            } catch (e) {
                console.log('Wrong response!' + e.message);
            }
            showCards(cards);
        }
    }
}

function showCards(cards) {
    cards.forEach(function (card) {
        let content = document.querySelector('.cards-section');
        content.innerHTML += cardGenerator.createCard(card);
    })
}

